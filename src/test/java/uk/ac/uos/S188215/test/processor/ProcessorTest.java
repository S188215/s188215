package uk.ac.uos.S188215.test.processor;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import org.junit.jupiter.api.Test;

import uk.ac.uos.S188215.processor.Processor;

class ProcessorTest {

	@Test
	void testPlainText() throws IOException {
		Properties props = new Properties();
		StringReader readfiles = new StringReader("hello");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("hello", result);
	}

	@Test
	void testSubstitution() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		StringReader readfiles = new StringReader("Hello $[name]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Hello Frank", result);
	}

	@Test
	void testMultipleSubstitution() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		props.put("date", "2019-05-17");
		StringReader readfiles = new StringReader("Hello $[name] $[date]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Hello Frank 2019-05-17", result);
	}

	@Test
	void testMultipleTheSame() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		StringReader readfiles = new StringReader("Hello $[name] $[name]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Hello Frank Frank", result);
	}

	@Test
	void testMultipleTheSame2() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		StringReader readfiles = new StringReader("Hello $[name]. How are you today $[name]?");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Hello Frank. How are you today Frank?", result);
	}

	@Test
	void testMissingKey() throws IOException {
		Properties props = new Properties();
		StringReader readfiles = new StringReader("hello $[name]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("hello *Key Missing*", result);
	}

	@Test
	void testMissingValue() throws IOException {
		Properties props = new Properties();
		props.put("name", "");
		StringReader readfiles = new StringReader("hello $[name]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("hello *Value Missing*", result);
	}

	@Test
	void testMoney() throws IOException {
		Properties props = new Properties();
		props.put("price", "5");
		StringReader readfiles = new StringReader("This product is $$[price]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("This product is $5", result);
	}

	@Test
	void testWhiteSpace() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		StringReader readfiles = new StringReader("hello $[name]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("hello Frank", result);
	}

	@Test
	void testPlaceholder() throws IOException {
		Properties props = new Properties();
		props.put("yourname", "Frank");
		StringReader readfiles = new StringReader("hello $[name]");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("hello *Key Missing*", result);
	}

	@Test
	void testManyValues() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		props.put("age", "21");
		props.put("height_m", "1.7");
		StringReader readfiles = new StringReader(
				"Hello you are $[name]. Welcome to this website. " + "You are $[age] years old and $[height_m]m tall.");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Hello you are Frank. Welcome to this website. " + "You are 21 years old and 1.7m tall.", result);

	}

	@Test
	void testLines() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		props.put("age", "21");
		props.put("height_m", "1.7");
		StringReader readfiles = new StringReader("Hello you are $[name]. Welcome to this website.\r\n"
				+ "You are $[age] years old and $[height_m]m tall.");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Hello you are Frank. Welcome to this website.\r\n" + "You are 21 years old and 1.7m tall.",
				result);

	}

	@Test
	void testBlankTemplate() throws IOException {
		Properties props = new Properties();

		props.put("name", "Frank");
		props.put("age", "21");
		props.put("height_m", "1.7");
		StringReader readfiles = new StringReader("");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("", result);

	}

	@Test
	void testOutputPrint() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		props.put("age", "21");
		props.put("height_m", "1.7");
		StringReader readfiles = new StringReader("$[name], you are $[age] and $[height_m]m tall.");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Frank, you are 21 and 1.7m tall.", result);
		System.out.println(result.toString());

	}

	@Test
	void testOutputToFile() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		props.put("age", "21");
		props.put("height_m", "1.7");
		StringReader readfiles = new StringReader("$[name], you are $[age] and $[height_m]m tall.");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Frank, you are 21 and 1.7m tall.", result);

		BufferedWriter bw = new BufferedWriter(new FileWriter("src/test/output/processor/output.txt"));
		bw.write(result);
		bw.close();
		System.out.println("\r\nWritten to src/test/output/processor/output.txt");
		
	}
	//means that application has functionalty to use a writer
	
	@Test
	void testOutputToHTML() throws IOException {
		Properties props = new Properties();
		props.put("name", "Frank");
		props.put("age", "21");
		props.put("height_m", "1.7");
		StringReader readfiles = new StringReader("$[name], you are $[age] and $[height_m]m tall.");
		Processor p = new Processor(props, readfiles);
		String result = p.getResult();
		assertEquals("Frank, you are 21 and 1.7m tall.", result);

		BufferedWriter bw = new BufferedWriter(new FileWriter("src/test/output/processor/output.html"));
		bw.write(result);
		bw.close();
		System.out.println("\r\nWritten to src/test/output/processor/output.html");
		
	}
	
	
}
