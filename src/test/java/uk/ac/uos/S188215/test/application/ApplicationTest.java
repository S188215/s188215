package uk.ac.uos.S188215.test.application;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

import org.junit.jupiter.api.Test;

import uk.ac.uos.S188215.application.Application;

class ApplicationTest {

	@Test
	void testPropsExists() {
		try {
			Application.application("testdata/testfail", "testdata/testtemps", "testdata/testresults");
			assert(false);
		} catch(Exception e) {
			assertEquals("Properties directory does not exist", e.getMessage());
		}
	}

	@Test
	void testTempsExists() {
		try {
			Application.application("testdata/testprops", "testdata/testfail", "testdata/testresults");
			assert(false);
		} catch(Exception e) {
			assertEquals("Templates directory does not exist", e.getMessage());
		}
	}

	@Test
	void testOutsExists() {
		try {
			Application.application("testdata/testprops", "testdata/testtemps", "testdata/testfail");
			assert(false);
		} catch(Exception e) {
			assertEquals("Results directory does not exist", e.getMessage());
		}
	}

	@Test
	void testMissingTemplate() throws Exception {
		// Read properties file
		Properties props = new Properties();
		FileReader propsReader = new FileReader("testdata/testpropsfail/customer2.txt");
		props.load(propsReader);
		propsReader.close();

		// Check corresponding template file exists.
		File t = new File("testdata/testtemps");
		String template = (String) props.getProperty("template");
		File templateFile = new File(t, template + ".txt");
		try {
			Application.process(templateFile, props, "src/main/java/results", "customer1.txt");
			assert(false);
		} catch(Exception e) {
			assertEquals("Template file missing", e.getMessage());
		}
	}

	@Test
	void testProcess() throws Exception {
		// Read properties file
		Properties props = new Properties();
		FileReader propsReader = new FileReader("testdata/testprops/customer1.txt");
		props.load(propsReader);
		propsReader.close();

		// Check corresponding template file exists.
		File t = new File("testdata/testtemps");
		String template = (String) props.getProperty("template");
		File templateFile = new File(t, template + ".txt");
		try {
			Application.process(templateFile, props, "src/main/java/results", "customer1.txt");
			assert(true);
		} catch(Exception e) {
			assert(false);
		}
	}

	@Test
	void testApplication() throws Exception {
		try {
			Application.application("testdata/testprops", "testdata/testtemps", "testdata/testresults");
			assert(true);
		} catch(Exception e) {
			assert(false);
		}
	}
}