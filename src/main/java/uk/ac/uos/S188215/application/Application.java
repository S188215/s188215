package uk.ac.uos.S188215.application;

import java.io.*;
import java.util.Properties;

import uk.ac.uos.S188215.processor.Processor;

public class Application {

	public static void main(String[] args) {
		String propertiesFolder = "src/main/java/properties";
		String templateFolder = "src/main/java/templates";
		String resultsFolder = "src/main/java/results";
		
		try {
			application(propertiesFolder, templateFolder, resultsFolder);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Done");

	} // end of main.

	public static void application(String propertiesFolder, String templateFolder, String resultsFolder) throws Exception {
		// Initialises variables
		String template = null;
		String[] paths = null;

		// Check directories exist
		File f = new File(propertiesFolder);
		if (!f.exists()) {
			throw new Exception("Properties directory does not exist");
		}

		
		File t = new File(templateFolder);
		if (!t.exists()) {
			throw new Exception("Templates directory does not exist");
		}

	
		if (!(new File(resultsFolder)).exists()) {
			throw new Exception("Results directory does not exist");
		}

		// array of files and directory
		paths = f.list();

		// for each name in the path array
		for (String path : paths) {

			// Read properties file
			Properties props = new Properties();
			FileReader propsReader = new FileReader(propertiesFolder + "/" + path);
			props.load(propsReader);
			propsReader.close();

			// Check corresponding template file exists.
			template = (String) props.getProperty("template");
			File templateFile = new File(t, template + ".txt");
			process(templateFile, props, resultsFolder, path);

		}
	}
	
	public static void process(File templateFile, Properties props, String resultsFolder, String path) throws Exception {
		if (templateFile.exists()) {
			
			// Process template and properties
			Processor pro = new Processor(props, new FileReader(templateFile));
			String result = pro.getResult();

			// Setup write location
			BufferedWriter bw = new BufferedWriter(
					new FileWriter(resultsFolder + "/" + path.split("\\.")[0] + ".html"));
			bw.write(result);
			bw.close();
			
		} else {
			throw new Exception("Template file missing");
		}
		
	}
}
