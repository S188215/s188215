package uk.ac.uos.S188215.processor;

import java.io.*;
import java.util.Properties;
import java.util.Set;

/**
 * 
 * Processor class for processing string input
 * 
 * @author s188215
 *
 */

public class Processor {
	// Initialise object attributes
	private final Properties props; 
	private final Reader readfiles;
	private final String result; 

{}

}

/**
	 * Constructor, this sets the initial values of the attributes.
	 * @param props
	 * @param readfiles
	 * @throws IOException
	 */
	// Constructor used to create a processor object.
	public Processor(Properties props, Reader readfiles) throws IOException {				
		this.props = props; 
		this.readfiles = readfiles;
		this.result = process(); 
	}

	/**
	 * process method, gets keys from properties and reads through template file
	 * character by character while it is not the end of the file. String builders 
	 * are created. If the character is a '$', continue reading, if it is then a '{',
	 * then continue reading. While it is not the end of of the file and the current key
	 * is not ']', then append the value of the key and continue reading. If it is 
	 * the end of the file, then break from the loop. If the key or value is missing,
	 * append an error message, else add the value from the properties and reset key
	 * length to 0. If the next character after '$' is not a '[', then append a $.
	 * If there is another '$' in the rest of the file, then repeat the loop. When
	 * it is the end of the file, close reader and return result.
	 * 
	 * @return
	 * @throws IOException
	 */
	private String process() throws IOException {
	    // Get keys from props
	    Set<Object> keys = props.keySet();

// Character by character step through template
	    char current = '\0';
	    StringBuilder key = new StringBuilder();
	    StringBuilder result = new StringBuilder();
    	current = (char) readfiles.read();
    	while(current != '\uffff') {
	    	if (current == '$') {
	    		current = (char) readfiles.read();
	    		if (current == '[') {
	        		current = (char) readfiles.read();
	        		// Get key
	    			while (current != '\uffff' && current != ']') {
	    				key.append(current);
	            		current = (char) readfiles.read();
	            	}

// Check not end of file
	    			if (current == '\uffff') break;
	    			
	    			// Check key exists
	    			if (!keys.contains(key.toString())) {
		    			result.append("*Key Missing*");
	    			} else if (((String)props.getProperty(key.toString())).isEmpty()) {
		    			result.append("*Value Missing*");
	    		    } else {
		    			// Add value from properties if key exists
		    			result.append((String)props.getProperty(key.toString()));
	    			}
	    			key.setLength(0);
            		current = (char) readfiles.read();
	    			
	        	} else {
	        		result.append('$');
	        	}
	    	} else {
	    		result.append(current);
		    	current = (char) readfiles.read();
	    	}
	    }
	    readfiles.close();
	    return result.toString();
	}
	
	/**
	 * getResult, 
	 * @return
	 */
	public String getResult() {  
		return result;
	}
}